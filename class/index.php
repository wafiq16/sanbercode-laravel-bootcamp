<?php
    // SOAL 1
    // include 'animal.php';
    // include 'Ape.php';
    include 'Frog.php';

    $sheep = new Animal("shaun");

    echo $sheep->name. "<br>"; // "shaun"
    echo $sheep->legs. "<br>"; // 4
    echo $sheep->cold_blooded. "<br>"; // "no"

    // SOAL 2

    $sungokong = new Ape("kera sakti");
    $sungokong->yell(); // "Auooo"

    $kodok = new Frog("buduk");
    $kodok->jump() ; // "hop hop"
?>